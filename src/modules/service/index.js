/**
 * @ngdoc shure.app.services
 * @name ShService
 * @description
 *  shService description
 *
 * @requires required module
 *
 * @example
 *  Register:
 *      import _shService from "PATH/service"
 *      angular.module('app')
 *          .service('shSomeService', _shService);
 */

class DataService {
    constructor($http) {
        this.$http = $http;
    }
    getFullName() {
        return this.$http.get('api/user/details');
    }
    getFirstName() {
        return this.$http.get('api/user/details');
    }
}

DataService.$inject = ['$http'];

export default DataService;