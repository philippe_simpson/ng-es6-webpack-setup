import _module from './index';

describe('Service example', function() {
    let service, $httpBackend;

    angular.module('app').service('shService', _module);

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject(function(_$httpBackend_, _shService_) {
        $httpBackend = _$httpBackend_;
        service = _shService_;
    }));

    it('getFullName() should get "userX"', function() {
        $httpBackend.expectGET('api/user/details').respond('userX');
        service.getFullName().then(function(data) {
            expect(data.data).toEqual('userX');
        });
        $httpBackend.flush();
    });
});
