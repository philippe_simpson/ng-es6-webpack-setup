import _component from './index';

describe('Component example', function() {
    var component, scope;

    //angular.module('app', []);
    angular.module('app').component('shComponent', _component);

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject(function(_$componentController_, _$rootScope_) {
        scope = _$rootScope_.$new();
        component = _$componentController_('shComponent', {$scope: scope}); // It's necessary to always pass the scope in the locals, so that the controller instance can be bound to it
    }));

    it('should set the defined name', function() {
        expect(component.name).toEqual("World");
    });
});