/**
 * @ngdoc shure.app.components
 * @name ShSomeComponent
 * @description
 *  ShSomeComponent description
 *
 * @requires required module
 *
 * @example
 *  Register:
 *      import _shComponent from "PATH/component";
 *      angular.module('app')
 *          .component('shSomeComponent', _shComponent);
 *
 *  Example:
 *      <sh-some-component></sh-some-component>
 */

// module dependencies:
import './component.scss';
import _template from './component.html';

export default {
    // Components are always isolated (i.e. scope: {}) and are always restricted to elements (i.e. restrict: 'E').
    bindings: { // define what to pass down to the isolate scope of the component.
        count: '='
    },
    controller: function () {
        this.name = 'World'; // controllerAs definition automatically defaults to $ctrl, so we can use $ctrl.name in our template.

        /*function doSomething(x) {
            // e.g. this.count++;
            return x + 2;
        }*/
    },
    template: _template
}
