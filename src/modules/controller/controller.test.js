import _module from './index';

describe('Controller example', function() {
    var ctrl, scope;

    //angular.module('app', []);
    angular.module('app').controller('shController', _module);

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject(function(_$controller_, _$rootScope_) {
        scope = _$rootScope_.$new();

        ctrl = _$controller_('shController', {$scope: scope}); // It's necessary to always pass the scope in the locals, so that the controller instance can be bound to it

        // scope.$digest(); // Run a manual $digest to resolve all promises if we have any mocked services.
    }));

    it('should set the defined name', function() {
        expect(ctrl.name).toEqual("World");
    });

});