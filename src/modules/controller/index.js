/**
 * @ngdoc shure.app.controllers
 * @name ShSomeController
 * @description
 *  shSomeController description
 *
 * @requires required module
 *
 * @example
 *  Register:
 *      import _shController from "PATH/controller"
 *      angular.module('app')
 *          .controller('shSomeController', _shController);
 *
 *  Example:
 *      <div ng-controller='shSomeController'></div>
 */

class ShController {
    constructor($http) {
        this.greet = 'Hello';
        this.name = 'World';
    }

    changeName() {
        this.name = 'angular-tips';
    }
}

ShController.$inject = ['$http'];

export default ShController;