// module dependencies:
import './directive.scss';
import _template from './directive.html';
import _controller from './controller';

export default function () {
    return {
        scope: {
            name: '@'
        },
        controller: _controller,
        controllerAs: 'ctrl',
        bindToController: true,
        replace: true,
        template: _template
    }
}