export default class ShController {
    constructor() {
        // When using ES6 we will use controllerAs so we put our stuff in this.
        this.greet = 'Hello';
        //this.name = 'World';
    }

    changeName() {
        this.name = 'angular-tips';
    }
}
