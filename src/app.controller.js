/**
 * @ngdoc shure.app.controllers
 * @name ShSomeController
 * @description
 *  shSomeController description
 *
 * @requires required module
 *
 * @example
 *  Register:
 *      import _shController from "PATH/controller"
 *      angular.module('app')
 *          .controller('shSomeController', _shController);
 *
 *  Example:
 *      <div ng-controller='shSomeController'></div>
 */

class AppCtrl {
    constructor($scope, $timeout, $mdSidenav, $log) {
        $scope.toggleLeft = buildDelayedToggler('left');
        $scope.toggleRight = buildToggler('right');
        $scope.isOpenRight = function(){
            return $mdSidenav('right').isOpen();
        };
        /**
         * Supplies a function that will continue to operate until the
         * time is up.
         */
        function debounce(func, wait) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildDelayedToggler(navID) {
            return debounce(function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug('toggle ' + navID + ' is done');
                    });
            }, 200);
        }
        function buildToggler(navID) {
            return function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug('toggle ' + navID + ' is done');
                    });
            }
        }
    }
}

class LeftCtrl {
     constructor($scope, $timeout, $mdSidenav, $log) {
         $scope.close = function () {
             $mdSidenav('left').close()
                 .then(function () {
                     $log.debug('close LEFT is done');
                 });
         };
     }
}

class RightCtrl {
    constructor($scope, $timeout, $mdSidenav, $log)
    {
        $scope.close = function () {
            $mdSidenav('right').close()
                .then(function () {
                    $log.debug('close RIGHT is done');
                });
        };
    }
}

AppCtrl.$inject = ['$scope', '$timeout', '$mdSidenav', '$log'];
LeftCtrl.$inject = ['$scope', '$timeout', '$mdSidenav', '$log'];
RightCtrl.$inject = ['$scope', '$timeout', '$mdSidenav', '$log'];

export { AppCtrl, LeftCtrl, RightCtrl }