/**
 * @ngdoc shure.app.controllers
 * @name ShSomeController
 * @description
 *  shSomeController description
 *
 * @requires required module
 *
 * @example
 *  Register:
 *      import _shController from "PATH/controller"
 *      angular.module('app')
 *          .controller('shSomeController', _shController);
 *
 *  Example:
 *      <div ng-controller='shSomeController'></div>
 */

class DummyDataController {
    // dummy data
    constructor() {
        this.mainNav = [
            {label: 'Home', icon: 'sh-icon-home'},
            {label: 'Inventories', icon: 'sh-icon-dashboard'},
            {label: 'Users', icon: 'sh-icon-users'},
            {label: 'Settings', icon: 'sh-icon-asterisk'}
        ];

        this.notifications = [
            {txt: 'Kanal 2 missing'},
            {txt: 'MXWAPT4-PDE missing'},
            {txt: 'Channel 1 missing'}
        ];

        this.userSettings = [
            {label: 'Account'},
            {label: 'Settings'},
            {label: 'Signout'}
        ];
    }
}

export default DummyDataController;