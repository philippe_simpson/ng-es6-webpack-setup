import angular from 'angular';
import ngMaterial from 'angular-material';
import 'angular-material/angular-material.css';
import './base.scss';
import 'shure-htmlclient/sh-typography';

import mdThemingProvider from 'shure-htmlclient/sh-theming/angular.module';

const appModuleName = 'app';

// Create application module
angular.module(appModuleName, [ngMaterial]);

// Configure MD theme on angular application module
mdThemingProvider(angular.module(appModuleName));

// temp test module dependencies:
import shIcons from 'shure-htmlclient/sh-icons/angular.module';
// Add sh-icons to angular module
shIcons(angular.module(appModuleName));

import shToolbar from 'shure-htmlclient/sh-toolbar/angular.module';
shToolbar(angular.module(appModuleName));

/*
import shComponent from './modules/component';
angular.module(appModuleName).component('shComponent', shComponent);
*/

import dummyData from './data.controller';
import { AppCtrl, LeftCtrl, RightCtrl } from './app.controller';

angular.module(appModuleName)
    .controller('DummyDataController', dummyData)
    .controller('AppCtrl', AppCtrl)
    .controller('LeftCtrl', LeftCtrl)
    .controller('RightCtrl', RightCtrl);
