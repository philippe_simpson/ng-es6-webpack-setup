'use strict';

var wallabyWebpack = require('wallaby-webpack');

var webpackConfig = require('./webpack.make')({
    BUILD: false,
    TEST: true
});
/*
webpackConfig.externals = {
    // Use external version of Angular
    "angular": "angular"
};
*/
webpackConfig.entryPatterns = [
    // // tests and test helper will be webpack-ed entry points
    'src/**/*.test.js',
    'src/tests.webpack.js'
];

//delete webpackConfig.devtool;

var wallabyPostprocessor = wallabyWebpack(webpackConfig);

//var webpackPostprocessor = wallabyWebpack({
//    entryPatterns: [
//        'src/tests.webpack.js',
//        'src/**/*.test.js'
//    ],
//
//    externals: {
//        // Use external version of Angular instead of rebuilding it
//        "angular": "Angular"
//    },
//
//    module: {
//        loaders: [
//            {
//                test: /\.scss$/,
//                //loader: 'sass-loader'
//                loaders: ["css?sourceMap", "sass?sourceMap"]
//            },
//            {
//                test: /\.html$/,
//                loader: 'raw-loader'
//            }
//        ]
//    }
//});

module.exports = function (wallaby) {

    return {
        // set `load: false` to all source files and tests processed by webpack
        // - except external files, as they should not be loaded in browser, their wrapped versions will be loaded instead
        files: [
            { pattern: 'src/tests.webpack.js', load: false }, // we need the file to be webpack-ed as others to resolve all requires
            { pattern: 'src/**/*.test.js', ignore: true }, // {ignore: true} to completely exclude the file from being processed by wallaby.js
            { pattern: 'src/**/*.scss', load: false },
            { pattern: 'src/**/*.html', load: false },
            { pattern: 'src/**/*.js', load: false}
        ],

        tests: [
            { pattern: 'src/**/*.test.js', load: false }
        ],

        //debug: true,

        compilers: {
            '**/*.js': wallaby.compilers.babel()
        },

        env: {
            type: 'browser',
            runner: require('phantomjs2').path
        },

        testFramework: "jasmine",

        postprocessor: wallabyPostprocessor,

        setup: function () {
            // required to trigger test loading
            window.__moduleBundler.loadTests();
        }
    };
};