'use strict';

// Modules
var webpack = require('webpack');
var path = require('path');
var autoprefixer = require('autoprefixer');
var stylelint = require('stylelint');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = function makeWebpackConfig (options) {
  /**
   * Environment type
   * BUILD is for generating minified builds
   * TEST is for generating test builds
   */
  var BUILD = !!options.BUILD;
  var TEST = !!options.TEST;

  /**
   * Config
   * Reference: http://webpack.github.io/docs/configuration.html
   * This is the object where all configuration gets set
   */
  var config = {};

  /**
   * Entry
   * Reference: http://webpack.github.io/docs/configuration.html#entry
   * Should be an empty object if it's generating a test build
   * Karma will set this when it's a test build
   */
  if (TEST) {
    config.entry = {}
  } else {
    config.entry = {
      app: './src/app.js'
    }
  }

  /**
   * Output
   * Reference: http://webpack.github.io/docs/configuration.html#output
   * Should be an empty object if it's generating a test build
   * Karma will handle setting it up for you when it's a test build
   */
  if (TEST) {
    config.output = {}
  } else {
    //var __dirname = '/ng-es6-webpack-setup';
    console.log('config.output');

    config.output = {
      // Absolute output directory
      path: __dirname + '/public',

      // Output path from the view of the page
      // Uses webpack-dev-server in development
      publicPath: BUILD ? '/' : 'http://localhost:8080/',

      // Filename for entry points
      // Only adds hash in build mode
      filename: BUILD ? '[name].[hash].js' : '[name].bundle.js',

      // Filename for non-entry points
      // Only adds hash in build mode
      chunkFilename: BUILD ? '[name].[hash].js' : '[name].bundle.js'
    }
  }

  /**
   * Devtool
   * Reference: http://webpack.github.io/docs/configuration.html#devtool
   * Type of sourcemap to use per build type
   */
  if (TEST) {
    config.devtool = 'inline-source-map';
  } else if (BUILD) {
    config.devtool = 'source-map';
  } else {
    config.devtool = 'eval';
  }

  /**
   * Loaders
   * Reference: http://webpack.github.io/docs/configuration.html#module-loaders
   * List: http://webpack.github.io/docs/list-of-loaders.html
   * This handles most of the magic responsible for converting modules
   */

  // Initialize module
  config.module = {
    preLoaders: [],
    loaders: [{
      // TS LOADER
      // Reference: https://github.com/TypeStrong/ts-loader
      // Transpile .ts files using ts-loader
      // Compiles Typescript into ES5 code
      test: /\.ts$/,
      loader: 'ts',
      exclude: /node_modules/
    }, {
      // JS LOADER
      // Reference: https://github.com/babel/babel-loader
      // Transpile .js files using babel-loader
      // Compiles ES6 and ES7 into ES5 code
      test: /\.js$/,
      loader: 'babel',
      //exclude: /node_modules/,
      include: [
        path.resolve(__dirname, "src"),
        path.resolve(__dirname, "node_modules/shure-htmlclient")
      ]
    }, {
      // CSS LOADER
      // Reference: https://github.com/webpack/css-loader
      // Allow loading css through js
      test: /\.css$/,
      // Reference: https://github.com/webpack/extract-text-webpack-plugin
      // Extract css files
      //
      // Reference: https://github.com/webpack/style-loader
      // Use style-loader
      loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
    },
      {
      // ASSET LOADER
      // Reference: https://github.com/webpack/file-loader
      // Copy png, jpg, jpeg, gif, svg, woff, woff2, ttf, eot files to output
      // Rename the file using the asset hash
      // Pass along the updated reference to your code
      // You can add here any file extension you want to get copied to your output
      test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
      loader: 'file'
    }, {
      // HTML LOADER
      // Reference: https://github.com/webpack/raw-loader
      // Allow loading html through js
      test: /\.html$/,
      loader: 'raw'
    }]
  };

  // ISPARTA LOADER
  // Reference: https://github.com/ColCh/isparta-instrumenter-loader
  // Instrument JS files with Isparta for subsequent code coverage reporting
  // Skips node_modules and files that end with .test.js
  if (TEST) {
    config.module.preLoaders.push({
      test: /\.js$/,
      exclude: [
        /node_modules/,
        /\.test\.js$/
      ],
      loader: 'isparta-instrumenter'
    })
  }

  // ESLint LOADER
  // Reference: https://github.com/MoOx/eslint-loader
  // Make Webpack emit ESLint messages
  // Skips node_modules and files that end with .test.js
  if (!TEST) {
    config.module.preLoaders.push(
        {
          test: /\.jsx?$/,
          loaders: ['eslint'],
          exclude: [
            /node_modules/,
            /\.test\.js$/
          ]/*,
          include: PATHS.app*/
        }
    )
  }

  // PostCSS LOADER
  // Reference: https://github.com/postcss/postcss-loader
  if (!TEST) {
    config.module.preLoaders.push(
        {
          test: /\.scss$/,
          loaders: ['postcss']
        }
    )
  }

      /**
       * PostCSS PLUGINS
       * Reference: https://github.com/postcss/autoprefixer-core
       * Add vendor prefixes to your css
       *
       * Reference: https://github.com/stylelint/stylelint
       * Enforce consistent conventions and avoid errors in your stylesheets.
       */
      config.postcss = [
        stylelint,
        autoprefixer({
          browsers: ['last 2 version']
        })
      ];

  // SCSS/CSS LOADER pipeline
  //
  // sass-loader
  // Reference: https://github.com/jtangelder/sass-loader
  // Returns compiled css code from file.scss, resolves Sass imports.
  //
  // css-loader
  // Reference: https://github.com/webpack/css-loader
  // Returns css code from file.css, resolves imports. By default the css-loader minimizes the css.
  //
  // style-loader
  // Reference: https://github.com/webpack/style-loader
  // Add rules in file.css to document.

  // To enable CSS Source maps, you'll need to pass the sourceMap-option to the sass- AND the css-loader

  var scssLoader = {
    test: /\.scss$/,
    loaders: ["style", "css?sourceMap", "sass?sourceMap"]//ExtractTextPlugin.extract('style-loader', scssLoaders.join('!'))
  };

  // Skip loading css in test mode
  if (TEST) {
    // Reference: https://github.com/webpack/null-loader
    // Return an empty module
    scssLoader = {
      test: /\.scss$/,
      loader: 'null'
    };//.loaders = 'null'
    //scssLoader.loader = 'null'
  }

  // Add scssLoader to the loader list
  config.module.loaders.push(scssLoader);

  /**
   * Plugins
   * Reference: http://webpack.github.io/docs/configuration.html#plugins
   * List: http://webpack.github.io/docs/list-of-plugins.html
   */
  config.plugins = [
    // Reference: https://github.com/webpack/extract-text-webpack-plugin
    // Extract css files
    // Disabled when in test mode or not in build mode
    new ExtractTextPlugin('[name].[hash].css', {
      disable: !BUILD || TEST
    })
  ];

  // Skip rendering index.html in test mode
  if (!TEST) {
    // Reference: https://github.com/ampedandwired/html-webpack-plugin
    // Render index.html
    config.plugins.push(
      new HtmlWebpackPlugin({
        template: './src/index.html',
        inject: 'body'
      })
    )
  }

  // Add build specific plugins
  if (BUILD) {
    config.plugins.push(
      // Reference: http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin
      // Only emit files when there are no errors
      new webpack.NoErrorsPlugin(),

      // Reference: http://webpack.github.io/docs/list-of-plugins.html#dedupeplugin
      // Dedupe modules in the output
      new webpack.optimize.DedupePlugin(),

      // Reference: http://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
      // Minify all javascript, switch loaders to minimizing mode
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false // it's currently only possible to turn off all warnings
        }
      })
    )
  }

  /**
   * Dev server configuration
   * Reference: http://webpack.github.io/docs/configuration.html#devserver
   * Reference: http://webpack.github.io/docs/webpack-dev-server.html
   */
  config.devServer = {
    contentBase: './public',
    stats: {
      modules: false,
      cached: false,
      colors: true,
      chunk: false
    }
  };

  return config;
};
