# Angular 1.5 + WebPack + Babel starter kit

This repo serves as a starting point for building Angular 1.5+ applications using ES6 modules with WebPack module bundler.

It is based on the [Angular workflow](https://github.com/preboot/angular-webpack/) of [Preboot](https://github.com/preboot).


The starter kit implements **Shure Shared Library** and offers example code using some library modules.
Shure Shared Library includes modules for MD theming, typography, icons and UI elements. 


## Features

* Heavily commented webpack configuration with reasonable defaults.
* ES6, and ES7 support with babel.js.
* Source maps included in all builds.
* Development server with live reload.
* Production builds with cache busting and asset minification.
* Testing environment using karma to run tests and jasmine as the framework.
* Code coverage when tests are run.
* No gulp and no grunt, just npm run-scripts.

Added:

* Scss loader and linter.
* ES6 linter.
* Wallaby.js support.
* TypeScript loader.
* Templates for Angular modules (Service, Component, Controller).

Bonus:

* Shure Shared Library
* App template.

## Installation

To use it, just clone this repo and install the npm dependencies:

```shell
$ git clone https://philippe_simpson@bitbucket.org/philippe_simpson/ng-es6-webpack-setup.git
$ cd ng-es6-webpack-setup
$ npm install
```

## Scripts

All scripts are run with `npm run [script]`, for example: `npm run test`.

* `build` - generate a minified build to dist folder
* `dev` - start development server, try it by opening `http://localhost:8080/`
* `test` - run all tests
* `test:live` - continuously run unit tests watching for changes

See what each script does by looking at the `scripts` section in [package.json](./package.json).

## Example and tutorial

To see how to structure an Angular 1.x application using this workflow, please check [this demo](https://github.com/Foxandxss/GermanWords-ng1-webpack).

Also, there is an article in [angular-tips](http://angular-tips.com/blog/2015/06/using-angular-1-dot-x-with-es6-and-webpack/)

## License

The license of this workflow is MIT.